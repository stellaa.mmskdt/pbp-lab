from django.db import models

# Create your models here.
# Create Note model that contains name, npm, and DOB (date of birth) here

class Note(models.Model):
    to = models.CharField(primary_key=True, max_length=30)
    # Implement missing attributes in Note model
    From = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.CharField(max_length=256)

    def __str__(self) -> str:
        return self.to + " - " + self.title