from django.db import models

# Create your models here.
# Create Friend model that contains name, npm, and DOB (date of birth) here

class Friend(models.Model):
    name = models.CharField(primary_key=True, max_length=30)
    # Implement missing attributes in Friend model
    npm = models.CharField(max_length=10)
    dob = models.DateField()

    def __str__(self) -> str:
        return self.name + " - " + self.npm